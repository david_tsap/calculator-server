### General
calculator server is a small java server that can accept simple mathematical calculations and return their results.
The server support 4 binary operations: 
*	addition (+)
*	substruction (-)
*	multiplication (*)
*	division (/)

change service port in application.properties file.

### API
POST /api/calc

Body
{
    "number1" : 10,
    "number2" : 5,
    "operation": "DIVISION"
}

Response
{
    "number1": 10.0,
    "number2": 5.0,
    "operation": "DIVISION",
    "result": 2.0
}
### Build
In root directory run:  
mvn clean install

### Run
After running compile using maven in target directory "accessControlService-0.0.1-SNAPSHOT.jar" will
be created.
go to target dir and run:  
java -jar calculator-0.0.1-SNAPSHOT.jar

### Setup
1. clone git repo
2. cd to root directory
3. run mvn clean install
4. cd target folder
5. run java -jar java -jar calculator-0.0.1-SNAPSHOT.jar