package com.calculator.calculator.constant;

public enum Operation{
  ADDITION,
  SUBTRACTION,
  MULTIPLICATION,
  DIVISION;

}
