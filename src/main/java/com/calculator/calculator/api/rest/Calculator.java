package com.calculator.calculator.api.rest;

import com.calculator.calculator.exception.CalculatorException;
import com.calculator.calculator.service.CalcService;
import com.calculator.calculator.servicemodel.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Calculator {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private CalcService calcService;

  @PostMapping("/api/calc")
  public ResponseEntity<Expression> calc(@RequestBody Expression expression) throws CalculatorException {
    logger.info("received request to calc " + expression);
    Float calc = calcService.calc(expression);
    expression.setResult(calc);
    logger.info("Response " + expression);
    return new ResponseEntity<>(expression, HttpStatus.OK);
  }
}
