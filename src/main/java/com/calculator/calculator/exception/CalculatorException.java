package com.calculator.calculator.exception;

public class CalculatorException extends Exception{

  protected ErrorCodeInterface errorCode;

  public CalculatorException(String massage) {
    super(massage);
  }

  public CalculatorException setErrorCode(ErrorCodeInterface errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  public ErrorCodeInterface getErrorCode() {
    return errorCode;
  }

}
