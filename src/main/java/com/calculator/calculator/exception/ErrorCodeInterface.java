package com.calculator.calculator.exception;

public interface ErrorCodeInterface {

  String code();

  String message();
}
