package com.calculator.calculator.exception;


public enum Errors implements ErrorCodeInterface {
  CALC_10000("CALC_10000", "operation not supported"),
  CALC_10001("CALC_10001", "divide by zero not allowed");


  private String code;
  private String message;

  private Errors(String code, String message) {
    this.code = code;
    this.message = message;
  }

  @Override
  public String code() {
    return this.code;
  }

  @Override
  public String message() {
    return this.message;
  }

}
