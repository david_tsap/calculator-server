package com.calculator.calculator.exception;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler{

    @ExceptionHandler(value
        = { CalculatorException.class})
    protected ResponseEntity<String> handleCalcException(
        CalculatorException ex) {
      ErrorCodeInterface bodyOfResponse = ex.getErrorCode();
      return new ResponseEntity<>(bodyOfResponse.code(), HttpStatus.BAD_REQUEST);
    }

  @ExceptionHandler(value={HttpMessageNotReadableException.class})
  public ResponseEntity<String> handleConflict(RuntimeException ex) {
    return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
  }

}