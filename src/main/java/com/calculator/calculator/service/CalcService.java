package com.calculator.calculator.service;

import com.calculator.calculator.exception.CalculatorException;
import com.calculator.calculator.exception.Errors;
import com.calculator.calculator.operations.OperationFactory;
import com.calculator.calculator.operations.operation;
import com.calculator.calculator.servicemodel.Expression;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalcService {

  @Autowired
  private OperationFactory operationFactory;

  public Float calc(Expression expression) throws CalculatorException {
    Optional<operation> service = operationFactory.getService(expression);
    if(service.isPresent()){
      return service.get().calc(expression.getNumber1(), expression.getNumber2());
    }else{
      throw new CalculatorException("operation not supported").setErrorCode(Errors.CALC_10000);
    }
  }
}
