package com.calculator.calculator.operations;

import com.calculator.calculator.servicemodel.Expression;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OperationFactory {

  @Autowired
  private List<operation> operationsServices;

  public Optional<operation> getService(Expression expression) {
    return operationsServices.stream()
        .filter(service -> service.getOperation().equals(expression.getOperation()))
        .findFirst();
  }
}
