package com.calculator.calculator.operations;

import com.calculator.calculator.constant.Operation;
import com.calculator.calculator.exception.CalculatorException;

public interface operation {

  Operation getOperation();

  float calc(float number1, float number2) throws CalculatorException;
}
