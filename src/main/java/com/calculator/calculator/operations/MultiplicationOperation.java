package com.calculator.calculator.operations;

import com.calculator.calculator.constant.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MultiplicationOperation implements operation{
  private final Logger logger = LoggerFactory.getLogger(getClass());
  @Override
  public Operation getOperation() {
    return Operation.MULTIPLICATION;
  }

  @Override
  public float calc(float number1, float number2) {
    logger.info("execute the following calc:" + number1 + " * " + number2);
    return number1 * number2;
  }
}
