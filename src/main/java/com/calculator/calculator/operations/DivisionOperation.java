package com.calculator.calculator.operations;

import com.calculator.calculator.constant.Operation;
import com.calculator.calculator.exception.CalculatorException;
import com.calculator.calculator.exception.Errors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DivisionOperation implements operation{
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public Operation getOperation() {
    return Operation.DIVISION;
  }

  @Override
  public float calc(float number1, float number2) throws CalculatorException {
    logger.info("execute the following calc:" + number1 + " / " + number2);
    if(number2 ==0){
      throw new CalculatorException("divide by zero not allowed").setErrorCode(Errors.CALC_10001);
    }
    return number1 / number2;
  }
}
